package main

//魚が食べられる
func (sakana *Fish) todoSakana() {
}

//魚を食べる
func (cat *Cat) eat(fish *Fish) {
}

//えさをとりに行く・・・・・未実装
type foodcather interface {
	foodget()
}

func (cat *StrayCat) foodget() {

}
func (cat *HouceCat) foodget() {
}

//猫クラス
type Cat struct {
}

//飼い主クラス
type Owner struct {
}

//飼い猫クラス
type HouceCat struct {
	Cat
	Owner
}

//野良猫クラス
type StrayCat struct {
	Cat
}

//お魚クラス
type Fish struct {
}

func main() {
	owner := &Owner{}
	houcecat := &HouceCat{Cat{}, *owner}
 	straycat := &StrayCat{Cat{}}
	sakana := &Fish{}

	houcecat.foodget()
	straycat.foodget()

	houcecat.eat(sakana)
}
