package cat

import (
	. "log"

	. "../Fish"
	. "../Meat"
	. "../Putable"
	. "../Stone"
)

type StomachState interface {
	Eat(value Putable) StomachState
	Talk()
	GetState() StomachState
}

func NewGood() *GoodStomachState {
	var GoodState *GoodStomachState
	return GoodState
}
func NewBad() *BadStomachState {
	var BadState *BadStomachState
	return BadState
}

type GoodStomachState struct {
}

func (State *GoodStomachState) pain() *StomachState {
	return State.GetState()
}
func (State *GoodStomachState) Talk() {
	Println("ごちそうさま")
}
func (State *GoodStomachState) Eat(value Putable) *StomachState {
	State.Talk()
	switch value.(type) {
	case *Stone: //石を食べたらお腹が痛くなる
		return State.pain()
	case *Meat, *Fish: //魚と肉は食べ物だ
	default:
	}
	return State.GetState()
}
func (State *GoodStomachState) GetState() *StomachState {
	var S StomachState
	return S
}

type BadStomachState struct {
}

func (State *BadStomachState) vomit() StomachState {
	return State.GetState()
}
func (State *BadStomachState) Talk() {
	Println("オェー")
}
func (State *BadStomachState) Eat(value Putable) StomachState {
	State.Talk()
	return State.vomit()
}
func (State *BadStomachState) GetState() StomachState {
	return NewBad()
}
