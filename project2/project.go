package main

import (
	. "./Cat"
	. "./Fish"
	. "./Meat"
	. "./Stone"
	//	. "fmt"
)

func main() {
	neco := NewCat()
	sakana := NewFish()
	niku := NewMeat()
	stone := NewStone()
	neco.Eat(sakana) //猫が魚を食べます
	neco.Eat(stone)  //猫が石を食べます
	neco.Eat(niku)   //食べれないから吐き出す
}
